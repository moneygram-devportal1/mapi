const axios = require('axios');
const { v4: uuidv4 } = require('uuid');

const createTransaction = async () => {

    // Step 1: Read configuration values with upmost security
    const token = "your_access_token_from_oauth_response"
    // For production - api.moneygram.com & For test - sandboxapi.moneygram.com    
    const host = "sandboxapi.moneygram.com";    
    const url = 'https://' + host + '/disbursement/v1/transactions';

    // Step 2: Create the POST request headers & body
    const headers = {
        'Content-Type': 'application/json',
        'X-MG-ClientRequestId': uuidv4(), // New UUID for each request tracing
        'Authorization': 'Bearer ' + token,
    };
    const request = {
        agentPartnerId: "your_partner_id",
        targetAudience: "AGENT_FACING",
        userLanguage: "en-US",
        destinationCountryCode: "USA",
        destinationCountrySubdivisionCode: "US-MN",
        serviceOptionCode: "WILL_CALL",
        sendAmount: {
            currencyCode: "USD",
            value: 500
        },
        receiveCurrencyCode: "USD",
        initiator: {
            method: "batch",
            userId: "abc",
            userType: "consumer"
        },
        receiver: {
            name: {
                firstName: "firstName",
                middleName: "",
                lastName: "lastName",
                secondLastName: ""
            }
        }
    }

    try {
        // Step 3: Send the request and obtain the response
        axios.post(url, request, { headers })
            .then(function (response) {
                // Step 4: Parse the success response and process further
                // Verify readyForCommit is true, if yes, transaction is ready to commit
                console.log(JSON.stringify(response.data, null, 2))
            })
            .catch(function (error) {
                // Step 5: Parse the error response and handle the errors
                if (error.response) {
                    console.log('Response status:', error.response.status);
                    console.log('Response body:', error.response.data);
                } else {
                    // TODO: handle generic errors
                    console.error('Error:', error.message);
                }
            });
    } catch (error) {
        // TODO: handle exception
        console.error('Error:', error.message);
    }
};

createTransaction();
