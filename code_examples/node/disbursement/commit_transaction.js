const axios = require('axios');
const { v4: uuidv4 } = require('uuid');

const commitTransaction = async () => {

    // Step 1: Read configuration values with upmost security
    const token = "your_access_token_from_oauth_response"
    // For production - api.moneygram.com & For test - sandboxapi.moneygram.com    
    const host = "sandboxapi.moneygram.com";
    const transactionId = "current_transaction_id";
    const url = 'https://' + host + '/disbursement/v1/transactions/' + transactionId + '/commit';

    // Step 2: Create the PUT request headers & body
    const headers = {
        'Content-Type': 'application/json',
        'X-MG-ClientRequestId': uuidv4(), // New UUID for each request tracing
        'Authorization': 'Bearer ' + token,
    };
    const request = {
        partnerTransactionId: ""
    }

    try {
        // Step 3: Send the request and obtain the response
        axios.put(url, request, { headers })
            .then(function (response) {
                // Step 4: Parse the success response and process further
                console.log(JSON.stringify(response.data, null, 2))
            })
            .catch(function (error) {
                // Step 5: Parse the error response and handle the errors
                if (error.response) {
                    console.log('Response status:', error.response.status);
                    console.log('Response body:', error.response.data);
                } else {
                    // TODO: handle generic errors
                    console.error('Error:', error.message);
                }
            });
    } catch (error) {
        // TODO: handle exception
        console.error('Error:', error.message);
    }
};

commitTransaction();
