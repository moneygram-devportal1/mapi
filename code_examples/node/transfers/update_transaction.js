const axios = require('axios');
const { v4: uuidv4 } = require('uuid');

const updateTransaction = async () => {

    // Step 1: Read configuration values with upmost security
    const token = "your_access_token_from_oauth_response"
    // For production - api.moneygram.com & For test - sandboxapi.moneygram.com
    const transactionId = "current_transaction_id";
    const host = "sandboxapi.moneygram.com";
    const url = 'https://' + host + '/transfer/v1/transactions/' + transactionId;

    // Step 2: Create the PUT request headers & body
    const headers = {
        'Content-Type': 'application/json',
        'X-MG-ClientRequestId': uuidv4(), // New UUID for each request tracing
        'Authorization': 'Bearer ' + token,
    };
    const request = {
        agentPartnerId: "your_partner_id",
        targetAudience: "AGENT_FACING",
        userLanguage: "en-US",
        destinationCountryCode: "USA",
        destinationCountrySubdivisionCode: "US-MN",
        serviceOptionCode: "WILL_CALL",
        sendAmount: {
            currencyCode: "USD",
            value: 500
        },
        receiveCurrencyCode: "USD",
        sender: {
            name: {
                firstName: "firstName",
                middleName: "",
                lastName: "lastName",
                secondLastName: ""
            },
            address: {
                line1: "line 1 of address",
                line2: "line 2 of address",
                line3: "",
                city: "minneapolis",
                countrySubdivisionCode: "US-TN",
                countryCode: "USA",
                postalCode: "53325"
            },
            mobilePhone: {
                number: "6123456789",
                countryDialCode: "1"
            },
            personalDetails: {
                genderCode: "MALE",
                dateOfBirth: "1980-08-15",
                birthCountryCode: "USA",
                citizenshipCountryCode: "USA"
            },
            primaryIdentification: {
                typeCode: "PAS",
                id: "passport_id_number",
                issueCountrySubdivisionCode: "US-MN",
                issueCountryCode: "USA"
            }
        },
        receiver: {
            name: {
                firstName: "firstName",
                middleName: "",
                lastName: "lastName",
                secondLastName: ""
            }
        }
    }

    try {
        // Step 3: Send the request and obtain the response
        axios.put(url, request, { headers })
            .then(function (response) {
                // Step 4: Parse the success response and process further
                // Verify readyForCommit is true, if yes, transaction is ready to commit
                console.log(JSON.stringify(response.data, null, 2))
            })
            .catch(function (error) {
                // Step 5: Parse the error response and handle the errors
                if (error.response) {
                    console.log('Response status:', error.response.status);
                    console.log('Response body:', error.response.data);
                } else {
                    // TODO: handle generic errors
                    console.error('Error:', error.message);
                }
            });
    } catch (error) {
        // TODO: handle exception
        console.error('Error:', error.message);
    }
};

updateTransaction();