const axios = require('axios');

const accessToken = async () => {

    // Step 1: Create the HTTP Headers and GET request
    const client_id = 'your_client_id';
    const client_secret = 'your_client_secret';
    // For production - api.moneygram.com & For test - sandboxapi.moneygram.com
    const host = "sandboxapi.moneygram.com";
    const url = 'https://' + host + '/oauth/accesstoken?grant_type=client_credentials';

    const encodedCredentials = Buffer.from(client_id + ':' + client_secret).toString('base64');
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + encodedCredentials,
    };

    try {
        // Step 2: Send the request and obtain the response
        axios.get(url, { headers })
            .then(function (response) {
                // Step 3: Parse the response and extract the access token
                const accessToken = response.data.access_token;
                console.log('Access Token:', accessToken);
                const expiresIn = response.data.expires_in;
                console.log('Token Expires In:', expiresIn);
            })
            .catch(function (error) {
                // Handle any errors that occurred during the request
                console.error('Error:', error.message);
            });

    } catch (error) {
        console.error('Error:', error.message);
    }
};

accessToken();
