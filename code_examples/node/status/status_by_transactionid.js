const axios = require('axios');
const { v4: uuidv4 } = require('uuid');

const status = async () => {

    // Step 1: Read configuration values with upmost security
    const token = "your_access_token_from_oauth_response"
    // For production - api.moneygram.com & For test - sandboxapi.moneygram.com
    const host = "sandboxapi.moneygram.com";
    const url = 'https://' + host + '/status/v1/transactions';

    // Step 2: Create the GET request headers & params
    const headers = {
        'Content-Type': 'application/json',
        'X-MG-ClientRequestId': uuidv4(), // New UUID for each request tracing
        'Authorization': 'Bearer ' + token,
    };
    
    const transactionId = "current_transaction_id";
    const params = {
        agentPartnerId: "your_partner_id",        
        userLanguage: "en-US",
        targetAudience: "AGENT_FACING"
    }

    try {
        // Step 3: Send the request and obtain the response
        axios.get(url + '/' + transactionId, { params, headers })
            .then(function (response) {
                // Step 4: Parse the success response and process further
                console.log(JSON.stringify(response.data, null, 2))
            })
            .catch(function (error) {
                // Step 5: Parse the error response and handle the errors
                if (error.response) {
                    console.log('Response status:', error.response.status);
                    console.log('Response body:', error.response.data);
                } else {
                    // TODO: handle generic errors
                    console.error('Error:', error.message);
                }
            });
    } catch (error) {
        // TODO: handle exception
        console.error('Error:', error.message);
    }
};

status();
