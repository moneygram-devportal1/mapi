const axios = require('axios');
const { v4: uuidv4 } = require('uuid');

const modifyReceiverAdditionalData = async () => {

    // Step 1: Read configuration values with upmost security
    const token = "your_access_token_from_oauth_response"
    // For production - api.moneygram.com & For test - sandboxapi.moneygram.com    
    const host = "sandboxapi.moneygram.com";    

    // Step 2: Create the PATCH request headers, params & body
    const headers = {
        'Content-Type': 'application/json',
        'X-MG-ClientRequestId': uuidv4(), // New UUID for each request tracing
        'Authorization': 'Bearer ' + token,
        'X-MG-SessionId': 'current_session_id'
    };

    params = {
        'agentPartnerId': 'your_partner_id',        
        'userLanguage': 'en-US',
        'targetAudience': 'AGENT_FACING'
    }

    const transactionId = "current_transaction_id";
    const url = 'https://' + host + '/amend/v1/transactions/' + transactionId + '/receiver/additional-data';

    const request = {
        'receiver': {
                'mobilePhone': {
                'number': '4592694333',
                'countryDialCode': '1'
            },
            'personalDetails': {
                'genderCode': 'MALE',
                'dateOfBirth': '1999-07-19',
                'birthCity': 'Albuquerque',
                'birthCountryCode': 'USA',
                'citizenshipCountryCode': 'USA',
                'occupationCode': 'ADMIN',
                'politicalExposedPerson': false
            },
            'familyDetails': [
                {
                    'type': 'FATHER_S_NAME',
                    'firstName': 'Roger',
                    'middleName': 'Kite',
                    'lastName': 'Walters',
                    'secondLastName': 'Junior'
                }
            ],
            'additionalDetails': [      
            ]
        }
    }

    try {
        // Step 3: Send the request and obtain the response
        axios.patch(url, request, { params, headers })
            .then(function (response) {
                // Step 4: Parse the success response and process further
                console.log(JSON.stringify(response.data, null, 2))
            })
            .catch(function (error) {
                // Step 5: Parse the error response and handle the errors
                if (error.response) {
                    console.log('Response status:', error.response.status);
                    console.log('Response body:', error.response.data);
                } else {
                    // TODO: handle generic errors
                    console.error('Error:', error.message);
                }
            });
    } catch (error) {
        // TODO: handle exception
        console.error('Error:', error.message);
    }
};

modifyReceiverAdditionalData();
