const axios = require('axios');
const { v4: uuidv4 } = require('uuid');

const createProfile = async () => {

    // Step 1: Read configuration values with upmost security
    const token = "your_access_token_from_oauth_response"
    // For production - api.moneygram.com & For test - sandboxapi.moneygram.com    
    const host = "sandboxapi.moneygram.com";    
    const url = 'https://' + host + '/consumer/v2/profiles';

    // Step 2: Create the POST request headers & body
    const headers = {
        'Content-Type': 'application/json',
        'X-MG-ClientRequestId': uuidv4(), // New UUID for each request tracing
        'Authorization': 'Bearer ' + token,
    };
    const request = {
        "targetAudience": "AGENT_FACING",
        "agentPartnerId": "your_partner_id",
        "userLanguage": "en-US",
        "consumer": {
            "firstName": "firstName",
            "middleName": "",
            "lastName": "lastName",
            "secondLastName": "",
            "address": {
                    "line1": "line1 of address",
                    "line2": "",
                    "line3": "",
                    "city": "minneapolis",
                    "countrySubdivisionCode": "US-MN",
                    "countryCode": "USA",
                    "postalCode": "55335"
                },
            "notificationPreferences": {
                    "type": "Transactional",
                    "channel": "SMS",
                    "optIn": true
                },
            "mobilePhone": {
                    "number": "6123456789",
                    "countryDialCode": "1"
                },
            "dateOfBirth": "1980-08-15"
        }
    }

    try {
        // Step 3: Send the request and obtain the response
        axios.post(url, request, { headers })
            .then(function (response) {
                // Step 4: Parse the success response and process further
                console.log(JSON.stringify(response.data, null, 2))
            })
            .catch(function (error) {
                // Step 5: Parse the error response and handle the errors
                if (error.response) {
                    console.log('Response status:', error.response.status);
                    console.log('Response body:', error.response.data);
                } else {
                    // TODO: handle generic errors
                    console.error('Error:', error.message);
                }
            });
    } catch (error) {
        // TODO: handle exception
        console.error('Error:', error.message);
    }
};

createProfile();
