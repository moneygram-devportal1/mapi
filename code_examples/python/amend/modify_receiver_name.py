import requests
import uuid
import json

def modifyReceiverName():

    # Step 1: Read configuration values with upmost security
    token = "your_access_token_from_oauth_response"
    # For production - api.moneygram.com & For test - sandboxapi.moneygram.com    
    host = "sandboxapi.moneygram.com";
    transactionId = "current_transaction_id";
    url = 'https://' + host + '/amend/v1/transactions/' + transactionId + '/receiver/name';

    # Step 2: Create the PATCH request headers, params & body
    headers = {
        'Content-Type': 'application/json',
        'X-MG-ClientRequestId': str(uuid.uuid4()), # New UUID for each request tracing
        'Authorization': 'Bearer ' + token,
        'X-MG-SessionId': 'current_session_id'
    }
    params = {
        'agentPartnerId': 'your_partner_id',        
        'userLanguage': 'en-US',
        'targetAudience': 'AGENT_FACING',
    }
    request = {
        'receiver': {
            'name': {
                'firstName': 'Ricky',
                'middleName': 'Livingstone',
                'lastName': 'Ben',
                'secondLastName': 'Junior'

            }
        }
    }

    try:
        # Step 3: Send the request and obtain the response
        response = requests.patch(url, json=request, headers=headers, params=params)

        # Step 4: Parse the success response and process further
        if response.status_code == 200:
            parsed_response = json.dumps(json.loads(response.text), indent=2)
            print(parsed_response)
        else:
            # Step 5: Parse the error response and handle the errors
            print("Request failed with status code:", response.status_code)
            print(json.dumps(json.loads(response.text), indent=2))

    except requests.exceptions.RequestException as e:
        # Print any error that occurred during the request
        # TODO: handle exception
        print("An error occurred:", e)

modifyReceiverName()