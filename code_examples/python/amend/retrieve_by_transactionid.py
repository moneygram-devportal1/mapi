import requests
import uuid
import json

def retrieve():

    # Step 1: Read configuration values with upmost security
    token = "your_access_token_from_oauth_response"
    # For production - api.moneygram.com & For test - sandboxapi.moneygram.com
    host = "sandboxapi.moneygram.com";
    url = 'https://' + host + '/amend/v1/transactions';

    # Step 2: Create the GET request headers & params
    headers = {
        'Content-Type': 'application/json',
        'X-MG-ClientRequestId': str(uuid.uuid4()), # New UUID for each request tracing
        'Authorization': 'Bearer ' + token,
    }

    transactionId = "current_transaction_id";
    params = {
        'agentPartnerId': 'your_partner_id',        
        'userLanguage': 'en-US',
        'targetAudience': 'AGENT_FACING',
    }

    try:
        # Step 3: Send the request and obtain the response
        response = requests.get(url + '/' + transactionId, params=params, headers=headers)

        # Step 4: Parse the success response and process further
        if response.status_code == 200:
            print("ResponseHeaders")
            print(response.headers)
            parsed_response = json.dumps(json.loads(response.text), indent=2)
            print("ResponseBody")
            print(parsed_response)            
        else:
            # Print the error message if request fails
            # TODO: handle exception
            print("Request failed with status code:", response.status_code)
            print(json.loads(json.dumps(response.text, indent=4)))

    except requests.exceptions.RequestException as e:
        # Print any error that occurred during the request
        # TODO: handle exception
        print("An error occurred:", e)

retrieve()