import requests
import uuid
import json

def transaction_history():

    # Step 1: Read configuration values with upmost security
    token = "your_access_token_from_oauth_response"
    # For production - api.moneygram.com & For test - sandboxapi.moneygram.com
    host = "sandboxapi.moneygram.com"
    url = 'https://' + host + '/consumer/v2/profiles'

    # Step 2: Create the GET request headers & params
    headers = {
        'Content-Type': 'application/json',
        'X-MG-ClientRequestId': str(uuid.uuid4()), # New UUID for each request tracing
        'Authorization': 'Bearer ' + token,
    }

    profileId = "your_profile_id"
    params = {
        'agentPartnerId': 'your_partner_id',    
        'userLanguage': 'en-US',
        'startDate': '2024-01-01',
        'endDate': '2024-01-16',
        'targetAudience': 'AGENT_FACING',
        'pageNumber': '1',
        'perPage': '20'
    }

    try:
        # Step 3: Send the request and obtain the response
        response = requests.get(url + '/' + profileId + '/transactions', params=params, headers=headers)

        # Step 4: Parse the success response and process further
        if response.status_code == 200:
            parsed_response = json.dumps(json.loads(response.text), indent=2)
            print(parsed_response)
        else:
            # Print the error message if request fails
            # TODO: handle exception
            print("Request failed with status code:", response.status_code)
            print(json.loads(json.dumps(response.text, indent=4)))

    except requests.exceptions.RequestException as e:
        # Print any error that occurred during the request
        # TODO: handle exception
        print("An error occurred:", e)

transaction_history()