import requests
import uuid
import json

def create_profile():

    # Step 1: Read configuration values with upmost security
    token = "your_access_token_from_oauth_response"
    # For production - api.moneygram.com & For test - sandboxapi.moneygram.com
    host = "sandboxapi.moneygram.com"    
    url = 'https://' + host + '/consumer/v2/profiles'

    # Step 2: Create the POST request headers & body
    headers = {
        'Content-Type': 'application/json',
        'X-MG-ClientRequestId': str(uuid.uuid4()), # New UUID for each request tracing
        'Authorization': 'Bearer ' + token,
    }
    request = {
        'targetAudience': 'AGENT_FACING',
        'agentPartnerId': 'your_partner_id',
        'userLanguage': 'en-US',
        'consumer': {
            'firstName': 'firstName',
            'middleName': '',
            'lastName': 'lastName',
            'secondLastName': '',
            'address': {
                    'line1': 'line1 of address',
                    'line2': '',
                    'line3': '',
                    'city': 'minneapolis',
                    'countrySubdivisionCode': 'US-MN',
                    'countryCode': 'USA',
                    'postalCode': '55335'
                },
            'notificationPreferences': {
                    'type': 'Transactional',
                    'channel': 'SMS',
                    'optIn': True
                },
            'mobilePhone': {
                    'number': '6123456789',
                    'countryDialCode': '1'
                },
            'dateOfBirth': '1980-08-15'
        }
    }

    try:
        # Step 3: Send the request and obtain the response
        response = requests.post(url, json=request, headers=headers)

        # Step 4: Parse the success response and process further
        if response.status_code == 200:
            parsed_response = json.dumps(json.loads(response.text), indent=2)
            print(parsed_response)
        else:
            # Step 5: Parse the error response and handle the errors
            print("Request failed with status code:", response.status_code)
            print(json.dumps(json.loads(response.text), indent=2))

    except requests.exceptions.RequestException as e:
        # Print any error that occurred during the request
        # TODO: handle exception
        print("An error occurred:", e)

create_profile()