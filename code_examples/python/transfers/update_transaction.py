import requests
import uuid
import json

def update_transaction():
    # Step 1: Read configuration values with upmost security
    token = "your_access_token_from_oauth_response"
    # For production - api.moneygram.com & For test - sandboxapi.moneygram.com
    transactionId = "current_transaction_id";
    host = "sandboxapi.moneygram.com";
    url = 'https://' + host + '/transfers/v1/transactions/'+ transactionId;

    # Step 2: Create the POST request headers & body
    headers = {
        'Content-Type': 'application/json',
        'X-MG-ClientRequestId': str(uuid.uuid4()), # New UUID for each request tracing
        'Authorization': 'Bearer ' + token,
    }
    request = {
        'agentPartnerId': 'your_partner_id',
        'targetAudience': 'AGENT_FACING',
        'userLanguage': 'en-US',
        'destinationCountryCode': 'USA',
        'destinationCountrySubdivisionCode': 'US-MN',
        'serviceOptionCode': 'WILL_CALL',
        'sendAmount': {
            'currencyCode': 'USD',
            'value': 500
        },
        'receiveCurrencyCode': 'USD',
        'sender': {
                'name': {
                    'firstName': 'firstName',
                    'middleName': '',
                    'lastName': 'lastName',
                    'secondLastName': ''
                },
                'address': {
                    'line1': 'line 1 of address',
                    'line2': 'line 2 of address',
                    'line3': '',
                    'city': 'minneapolis',
                    'countrySubdivisionCode': 'US-TN',
                    'countryCode': 'USA',
                    'postalCode': '53325'
                },
                'mobilePhone': {
                    'number': '6123456789',
                    'countryDialCode': '1'
                },
                'personalDetails': {
                    'genderCode': 'MALE',
                    'dateOfBirth': '1980-08-15',
                    'birthCountryCode': 'USA',
                    'citizenshipCountryCode': 'USA'
                },
                'primaryIdentification': {
                    'typeCode': 'PAS',
                    'id': 'passport_id_number',
                    'issueCountrySubdivisionCode': 'US-MN',
                    'issueCountryCode': 'USA'
                }
            },
            'receiver': {
                'name': {
                    'firstName': 'firstName',
                    'middleName': '',
                    'lastName': 'lastName',
                    'secondLastName': ''
                }
            }
    }

    try:
        # Step 3: Send the request and obtain the response
        response = requests.put(url, json=request, headers=headers)

        # Step 4: Parse the success response and process further
        if response.status_code == 200:
            parsed_response = json.dumps(json.loads(response.text), indent=2)
            print(parsed_response)
        else:
            # Step 5: Parse the error response and handle the errors
            print("Request failed with status code:", response.status_code)
            print(json.dumps(json.loads(response.text), indent=2))

    except requests.exceptions.RequestException as e:
        # Print any error that occurred during the request
        # TODO: handle exception
        print("An error occurred:", e)

update_transaction()