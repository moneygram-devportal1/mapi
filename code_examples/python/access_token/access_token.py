import requests
import base64
import json

def access_token():

    # Step 1: Create the HTTP Headers and GET request
    client_id = 'your_client_id';
    client_secret = 'your_client_secret';
    # For production - api.moneygram.com & For test - sandboxapi.moneygram.com
    host = "sandboxapi.moneygram.com";
    url = 'https://' + host + '/oauth/accesstoken?grant_type=client_credentials';

    credentials = f"{client_id}:{client_secret}"
    encoded_credentials = base64.b64encode(credentials.encode('utf-8')).decode('utf-8')
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + encoded_credentials
    }

    try:
        # Step 2: Send the request and obtain the response
        response = requests.get(url, headers=headers)

        # Step 3: Parse the response and extract the access token
        if response.status_code == 200:
            parsed_response = json.loads(response.text)
            accessToken = parsed_response['access_token'];
            print("Access Token:"+ accessToken)
            expiresIn = parsed_response['expires_in'];
            print("Token Expires In:"+ expiresIn)
        else:
            # Print the error message if request fails
            print("Request failed with status code:", response.status_code)

    except requests.exceptions.RequestException as e:
        # Print any error that occurred during the request
        print("An error occurred:", e)

access_token()