package amend;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.UUID;

public class RetrieveByTransactionID {

    public static void main(String[] args) {
        // Step 1: Read configuration values with upmost security
        String token = "your_access_token_from_oauth_response";

        // For production - api.moneygram.com & For test - sandboxapi.moneygram.com
        String host = "sandboxapi.moneygram.com";

        // Step 2: Create the GET request headers & params
        // Mandatory Query params
        String agentPartnerId = "your_partner_id";
        String userLanguage = "en-US";

        // Optional Query params
        String targetAudience = "AGENT_FACING";

        // Mandatory Path params
        String transactionId = "current_transaction_id";

        String uri = "https://" + host + "/amend/v1/transactions/" + transactionId + "?"
                + "agentPartnerId=" + agentPartnerId
                + "&userLanguage=" + userLanguage
                + (targetAudience.isBlank() ? "" : "&targetAudience=" + targetAudience);

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .GET()
                .setHeader("Authorization", "Bearer " + token)
                .setHeader("X-MG-ClientRequestId", String.valueOf(UUID.randomUUID()))
                .build();

        try {
            // Step 3: Send the request and obtain the response
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            // Retrieve the status code and body from the response
            int statusCode = response.statusCode();

            // Step 4: Parse the success response and process further
            if (statusCode == 200) {
                String responseHeaders = String.valueOf(response.headers());
                System.out.println("ResponseHeaders\n" + responseHeaders);
                String responseBody = response.body();
                System.out.println("ResponseBody\n" + responseBody);
            } else {
                // Step 5: Parse the error response and handle the errors
                String responseBody = response.body();
                System.out.println(responseBody);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }

    }
}
