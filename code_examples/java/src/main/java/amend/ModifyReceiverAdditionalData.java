package amend;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import java.io.StringWriter;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.UUID;

public class ModifyReceiverAdditionalData {

    public static void main(String[] args) {
        // Step 1: Read configuration values with upmost security
        String token = "your_access_token_from_oauth_response";

        // For production - api.moneygram.com & For test - sandboxapi.moneygram.com
        String host = "sandboxapi.moneygram.com";

        // Step 2: Create the PATCH request headers, params & body
        // Mandatory Query params
        String agentPartnerId = "your_partner_id";
        String targetAudience = "AGENT_FACING";
        String userLanguage = "en-US";

        // Mandatory Path params
        String transactionId = "current_transaction_id";

        String uri = "https://" + host + "/amend/v1/transactions/" + transactionId + "/receiver/additional-data" + "?"
                + "agentPartnerId=" + agentPartnerId
                + "&targetAudience=" + targetAudience
                + "&userLanguage=" + userLanguage;

        // Create a JSON object
        JsonObjectBuilder requestBuilder = Json.createObjectBuilder()
                .add("receiver",
                        Json.createObjectBuilder().add("mobilePhone",
                                        Json.createObjectBuilder().add("number", "4592694333")
                                                .add("countryDialCode", "1"))
                                .add("personalDetails",
                                        Json.createObjectBuilder().add("genderCode", "MALE")
                                                .add("dateOfBirth", "1999-07-19")
                                                .add("birthCity", "Albuquerque")
                                                .add("birthCountryCode", "USA")
                                                .add("citizenshipCountryCode", "USA")
                                                .add("occupationCode", "ADMIN")
                                                .add("politicalExposedPerson", false))
                                .add("familyDetails",
                                        Json.createArrayBuilder().add(Json.createObjectBuilder().add("type", "FATHER_S_NAME")
                                                .add("firstName", "Roger")
                                                .add("middleName", "Kite")
                                                .add("lastName", "Walters")
                                                .add("secondLastName", "Junior")))
                                .add("additionalDetails", Json.createArrayBuilder()));

        JsonObject jsonObject = requestBuilder.build();
        // Create a StringWriter to write the JSON string
        StringWriter stringWriter = new StringWriter();
        try (JsonWriter jsonWriter = Json.createWriter(stringWriter)) {
            jsonWriter.writeObject(jsonObject);
        }
        // Get the JSON string from the StringWriter
        String jsonString = stringWriter.toString();

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .method("PATCH", HttpRequest.BodyPublishers.ofString(jsonString))
                .setHeader("Authorization", "Bearer " + token)
                .setHeader("X-MG-ClientRequestId", String.valueOf(UUID.randomUUID()))
                .setHeader("X-MG-SessionId", "current_session_id")
                .build();

        try {
            // Step 3: Send the request and obtain the response
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            // Retrieve the status code and body from the response
            int statusCode = response.statusCode();

            // Step 4: Parse the success response and process further
            if (statusCode == 200) {
                String responseBody = response.body();
                System.out.println(responseBody);
            } else {
                // Step 5: Parse the error response and handle the errors
                String responseBody = response.body();
                System.out.println(responseBody);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
    }
}
