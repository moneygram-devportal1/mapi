package consumer;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import java.io.StringWriter;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.UUID;

public class CreateProfile {

    public static void main(String[] args) {
        // Step 1: Read configuration values with upmost security
        String token = "your_access_token_from_oauth_response";

        // For production - api.moneygram.com & For test - sandboxapi.moneygram.com
        String host = "sandboxapi.moneygram.com";
        String uri = "https://" + host + "/consumer/v2/profiles";

        // Step 2: Create the POST request headers & body
        // Create a JSON object
        JsonObjectBuilder requestBuilder = Json.createObjectBuilder()
                .add("targetAudience", "AGENT_FACING")
                .add("agentPartnerId", "your_partner_id")
                .add("userLanguage", "en-US")
                .add("consumer", Json.createObjectBuilder().add("firstName", "firstName")
                        .add("middleName", "")
                        .add("lastName", "lastName")
                        .add("secondLastName", "")
                        .add("address", Json.createObjectBuilder().add("line1", "line1 of address")
                                .add("line2", "")
                                .add("line3", "")
                                .add("city", "minneapolis")
                                .add("countrySubdivisionCode", "US-MN")
                                .add("countryCode", "USA")
                                .add("postalCode", "55335"))
                        .add("notificationPreferences", Json.createObjectBuilder().add("type", "Transactional")
                                .add("channel", "SMS")
                                .add("optIn", true))
                        .add("mobilePhone",
                                Json.createObjectBuilder().add("number", "6123456789")
                                        .add("countryDialCode", "1"))
                        .add("dateOfBirth", "1980-08-15")
                );

        JsonObject jsonObject = requestBuilder.build();
        // Create a StringWriter to write the JSON string
        StringWriter stringWriter = new StringWriter();
        try (JsonWriter jsonWriter = Json.createWriter(stringWriter)) {
            jsonWriter.writeObject(jsonObject);
        }
        // Get the JSON string from the StringWriter
        String jsonString = stringWriter.toString();

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .POST(HttpRequest.BodyPublishers.ofString(jsonString))
                .setHeader("Authorization", "Bearer " + token)
                .setHeader("X-MG-ClientRequestId", String.valueOf(UUID.randomUUID()))
                .build();

        try {
            // Step 3: Send the request and obtain the response
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            // Retrieve the status code and body from the response
            int statusCode = response.statusCode();

            // Step 4: Parse the success response and process further
            if (statusCode == 200) {
                String responseBody = response.body();
                System.out.println(responseBody);
            } else {
                // Step 5: Parse the error response and handle the errors
                String responseBody = response.body();
                System.out.println(responseBody);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
    }
}
