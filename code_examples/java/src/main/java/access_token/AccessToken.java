package access_token;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Base64;

public class AccessToken {

    public static void main(String[] args) {

        // Replace these values with your actual client credentials and token endpoint host
        String clientId = "your_client_id";
        String clientSecret = "your_client_secret";

        // For production - api.moneygram.com & For test - sandboxapi.moneygram.com
        String host = "sandboxapi.moneygram.com";
        String tokenEndpoint = "https://" + host + "/oauth/accesstoken?grant_type=client_credentials";

        // Step 1: Create the HTTP client and GET request
        HttpClient httpClient = HttpClient.newHttpClient();
        String credentials = clientId + ":" + clientSecret;
        String encodedCredentials = Base64.getEncoder().encodeToString(credentials.getBytes());
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(tokenEndpoint))
                .GET()
                .setHeader("Authorization", "Basic " + encodedCredentials)
                .build();

        try {
            // Step 2: Send the request and obtain the response
            HttpResponse<String> response = httpClient.send(request, BodyHandlers.ofString());

            // Retrieve the status code and body from the response
            int statusCode = response.statusCode();
            System.out.println("Status Code: " + statusCode);

            // Step 3: Parse the response and extract the access token
            if (statusCode == 200) {
                String responseBody = response.body();
                JsonReader reader = Json.createReader(new StringReader(responseBody));
                JsonObject jsonObject = reader.readObject();
                String accessToken = jsonObject.getString("access_token");
                System.out.println("Access Token: " + accessToken);
                String expiresIn = jsonObject.getString("expires_in");
                System.out.println("Token Expires In: " + expiresIn);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
    }
}

